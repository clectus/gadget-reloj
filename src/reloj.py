# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'reloj.ui'
##
## Created by: Qt User Interface Compiler version 6.5.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QLabel,
    QMainWindow, QPushButton, QSizePolicy, QWidget)

from .iconos import *

class Ui_Reloj(object):
    def setupUi(self, Reloj):
        if not Reloj.objectName():
            Reloj.setObjectName(u"Reloj")
        Reloj.resize(424, 268)
        self.wt_central = QWidget(Reloj)
        self.wt_central.setObjectName(u"wt_central")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.wt_central.sizePolicy().hasHeightForWidth())
        self.wt_central.setSizePolicy(sizePolicy)
        self.grid_central = QGridLayout(self.wt_central)
        self.grid_central.setObjectName(u"grid_central")
        self.grid_central.setHorizontalSpacing(0)
        self.grid_central.setVerticalSpacing(2)
        self.grid_central.setContentsMargins(0, 0, 0, 0)
        self.fr_barra = QFrame(self.wt_central)
        self.fr_barra.setObjectName(u"fr_barra")
        self.fr_barra.setMinimumSize(QSize(0, 42))
        self.fr_barra.setMaximumSize(QSize(16777215, 42))
        self.fr_barra.setFrameShape(QFrame.Panel)
        self.fr_barra.setFrameShadow(QFrame.Raised)
        self.grid_barra = QGridLayout(self.fr_barra)
        self.grid_barra.setObjectName(u"grid_barra")
        self.grid_barra.setContentsMargins(2, 4, 9, 9)
        self.lb_titulo = QLabel(self.fr_barra)
        self.lb_titulo.setObjectName(u"lb_titulo")
        self.lb_titulo.setIndent(7)

        self.grid_barra.addWidget(self.lb_titulo, 0, 0, 1, 1)

        self.bt_cerrar = QPushButton(self.fr_barra)
        self.bt_cerrar.setObjectName(u"bt_cerrar")
        self.bt_cerrar.setMinimumSize(QSize(31, 31))
        self.bt_cerrar.setMaximumSize(QSize(31, 31))
        self.bt_cerrar.setCursor(QCursor(Qt.PointingHandCursor))
        icon = QIcon()
        icon.addFile(u":/img/cerrar.ico", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_cerrar.setIcon(icon)
        self.bt_cerrar.setIconSize(QSize(32, 32))
        self.bt_cerrar.setFlat(True)

        self.grid_barra.addWidget(self.bt_cerrar, 0, 1, 1, 1)


        self.grid_central.addWidget(self.fr_barra, 0, 0, 1, 1)

        self.fr_hora = QFrame(self.wt_central)
        self.fr_hora.setObjectName(u"fr_hora")
        self.fr_hora.setFrameShape(QFrame.Panel)
        self.fr_hora.setFrameShadow(QFrame.Raised)
        self.grid_hora = QGridLayout(self.fr_hora)
        self.grid_hora.setObjectName(u"grid_hora")
        self.lb_hora = QLabel(self.fr_hora)
        self.lb_hora.setObjectName(u"lb_hora")
        self.lb_hora.setAlignment(Qt.AlignCenter)

        self.grid_hora.addWidget(self.lb_hora, 0, 0, 1, 1)

        self.bt_redes = QPushButton(self.fr_hora)
        self.bt_redes.setObjectName(u"bt_redes")
        self.bt_redes.setMinimumSize(QSize(31, 31))
        self.bt_redes.setMaximumSize(QSize(31, 31))
        self.bt_redes.setCursor(QCursor(Qt.PointingHandCursor))
        icon1 = QIcon()
        icon1.addFile(u":/img/mas.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_redes.setIcon(icon1)
        self.bt_redes.setIconSize(QSize(34, 31))
        self.bt_redes.setFlat(True)

        self.grid_hora.addWidget(self.bt_redes, 0, 1, 1, 1)


        self.grid_central.addWidget(self.fr_hora, 1, 0, 1, 1)

        self.fr_fecha = QFrame(self.wt_central)
        self.fr_fecha.setObjectName(u"fr_fecha")
        self.fr_fecha.setFrameShape(QFrame.Panel)
        self.fr_fecha.setFrameShadow(QFrame.Raised)
        self.grid_fecha = QGridLayout(self.fr_fecha)
        self.grid_fecha.setObjectName(u"grid_fecha")
        self.lb_fecha = QLabel(self.fr_fecha)
        self.lb_fecha.setObjectName(u"lb_fecha")
        self.lb_fecha.setAlignment(Qt.AlignCenter)

        self.grid_fecha.addWidget(self.lb_fecha, 0, 0, 1, 1)

        self.bt_ajustes = QPushButton(self.fr_fecha)
        self.bt_ajustes.setObjectName(u"bt_ajustes")
        self.bt_ajustes.setMinimumSize(QSize(31, 31))
        self.bt_ajustes.setMaximumSize(QSize(31, 31))
        icon2 = QIcon()
        icon2.addFile(u":/img/ajust.png", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_ajustes.setIcon(icon2)
        self.bt_ajustes.setIconSize(QSize(32, 32))
        self.bt_ajustes.setCheckable(False)
        self.bt_ajustes.setFlat(True)

        self.grid_fecha.addWidget(self.bt_ajustes, 0, 1, 1, 1)


        self.grid_central.addWidget(self.fr_fecha, 2, 0, 1, 1)

        self.fr_redes = QFrame(self.wt_central)
        self.fr_redes.setObjectName(u"fr_redes")
        self.fr_redes.setMaximumSize(QSize(16777215, 45))
        self.fr_redes.setFrameShape(QFrame.Panel)
        self.fr_redes.setFrameShadow(QFrame.Raised)
        self.grid_redes = QGridLayout(self.fr_redes)
        self.grid_redes.setObjectName(u"grid_redes")
        self.grid_redes.setContentsMargins(9, 7, -1, -1)
        self.bt_tw = QPushButton(self.fr_redes)
        self.bt_tw.setObjectName(u"bt_tw")
        self.bt_tw.setMinimumSize(QSize(31, 31))
        self.bt_tw.setMaximumSize(QSize(31, 31))
        self.bt_tw.setCursor(QCursor(Qt.PointingHandCursor))
        icon3 = QIcon()
        icon3.addFile(u":/img/twit.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_tw.setIcon(icon3)
        self.bt_tw.setIconSize(QSize(39, 39))
        self.bt_tw.setFlat(False)

        self.grid_redes.addWidget(self.bt_tw, 0, 0, 1, 1)

        self.bt_wh = QPushButton(self.fr_redes)
        self.bt_wh.setObjectName(u"bt_wh")
        self.bt_wh.setMinimumSize(QSize(31, 31))
        self.bt_wh.setMaximumSize(QSize(31, 31))
        self.bt_wh.setCursor(QCursor(Qt.PointingHandCursor))
        icon4 = QIcon()
        icon4.addFile(u":/img/wasa.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_wh.setIcon(icon4)
        self.bt_wh.setIconSize(QSize(31, 31))
        self.bt_wh.setFlat(False)

        self.grid_redes.addWidget(self.bt_wh, 0, 1, 1, 1)

        self.bt_tg = QPushButton(self.fr_redes)
        self.bt_tg.setObjectName(u"bt_tg")
        self.bt_tg.setMinimumSize(QSize(31, 31))
        self.bt_tg.setMaximumSize(QSize(31, 31))
        self.bt_tg.setCursor(QCursor(Qt.PointingHandCursor))
        icon5 = QIcon()
        icon5.addFile(u":/img/tele.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_tg.setIcon(icon5)
        self.bt_tg.setIconSize(QSize(31, 31))
        self.bt_tg.setFlat(False)

        self.grid_redes.addWidget(self.bt_tg, 0, 2, 1, 1)

        self.bt_tk = QPushButton(self.fr_redes)
        self.bt_tk.setObjectName(u"bt_tk")
        self.bt_tk.setMinimumSize(QSize(31, 31))
        self.bt_tk.setMaximumSize(QSize(31, 31))
        self.bt_tk.setCursor(QCursor(Qt.PointingHandCursor))
        icon6 = QIcon()
        icon6.addFile(u":/img/tick.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_tk.setIcon(icon6)
        self.bt_tk.setIconSize(QSize(39, 41))
        self.bt_tk.setFlat(False)

        self.grid_redes.addWidget(self.bt_tk, 0, 3, 1, 1)

        self.bt_ig = QPushButton(self.fr_redes)
        self.bt_ig.setObjectName(u"bt_ig")
        self.bt_ig.setMinimumSize(QSize(31, 31))
        self.bt_ig.setMaximumSize(QSize(31, 31))
        self.bt_ig.setCursor(QCursor(Qt.PointingHandCursor))
        icon7 = QIcon()
        icon7.addFile(u":/img/insta.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_ig.setIcon(icon7)
        self.bt_ig.setIconSize(QSize(38, 41))
        self.bt_ig.setFlat(False)

        self.grid_redes.addWidget(self.bt_ig, 0, 4, 1, 1)

        self.bt_fb = QPushButton(self.fr_redes)
        self.bt_fb.setObjectName(u"bt_fb")
        self.bt_fb.setMinimumSize(QSize(31, 31))
        self.bt_fb.setMaximumSize(QSize(31, 31))
        self.bt_fb.setCursor(QCursor(Qt.PointingHandCursor))
        icon8 = QIcon()
        icon8.addFile(u":/img/msg.svg", QSize(), QIcon.Normal, QIcon.Off)
        self.bt_fb.setIcon(icon8)
        self.bt_fb.setIconSize(QSize(31, 31))
        self.bt_fb.setFlat(False)

        self.grid_redes.addWidget(self.bt_fb, 0, 5, 1, 1)


        self.grid_central.addWidget(self.fr_redes, 3, 0, 1, 1)

        Reloj.setCentralWidget(self.wt_central)

        # self.retranslateUi(Reloj)

        QMetaObject.connectSlotsByName(Reloj)
    # setupUi

    # def retranslateUi(self, Reloj):
    #     Reloj.setWindowTitle(QCoreApplication.translate("Reloj", u"MainWindow", None))
    #     self.lb_titulo.setText("")
    #     self.bt_cerrar.setText("")
    #     self.lb_hora.setText("")
    #     self.bt_redes.setText("")
    #     self.lb_fecha.setText("")
    #     self.bt_ajustes.setText("")
    #     self.bt_tw.setText("")
    #     self.bt_wh.setText("")
    #     self.bt_tg.setText("")
    #     self.bt_tk.setText("")
    #     self.bt_ig.setText("")
    #     self.bt_fb.setText("")
    # retranslateUi

