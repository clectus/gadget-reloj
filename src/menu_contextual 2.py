
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *


class MainForm(QMainWindow):
    def __init__(self):
        super(MainForm, self).__init__()

        # create button
        self.button = QPushButton("test button", self)       
        self.button.resize(100, 30)

        # set button context menu policy
        self.button.setContextMenuPolicy(Qt.CustomContextMenu)
        self.button.customContextMenuRequested.connect(self.on_context_menu)

        # create context menu
        self.popMenu = QMenu(self)
        self.checkeable = QAction('&Checeable', self)
        self.checkeable.triggered.connect(self.on_chekeable)
        self.checkeable.setCheckable(True)
        self.popMenu.addAction(self.checkeable)
        self.popMenu.addAction(QAction('test1', self))
        self.popMenu.addSeparator()
        self.popMenu.addAction(QAction('test2', self))        

    def on_context_menu(self, point):
        # show context menu
        self.popMenu.exec(self.button.mapToGlobal(point))    

    def on_chekeable(self):
        if self.checkeable.isChecked():
            print('se clikeo')
        else:
            print('se desclikeo')

def main():
    app = QApplication([])
    form = MainForm()
    form.show()
    app.exec()

if __name__ == '__main__':
    main()