from PySide6.QtWidgets import QMainWindow, QApplication, QMenu, QPushButton
from PySide6.QtCore import QTimer, Qt, QSize
from PySide6.QtGui import QAction, QCursor, QIcon

from src.posicion_ventana import PosicionVentana

from src.reloj import Ui_Reloj

from locale import LC_ALL, setlocale
from time import strftime
from shutil import copy
from os import path, getenv, remove
from getpass import getuser
import webbrowser

class RjMetodos(QMainWindow, Ui_Reloj):
	"""docstring for RjMetodos"""
	def __init__(self):
		super(RjMetodos, self).__init__()

		self.setupUi(self)
		self.setFixedSize(480, 231)
		self.setWindowFlag(Qt.WindowType.FramelessWindowHint)
		self.setAttribute(Qt.WA_TranslucentBackground)
		# self.setStyleSheet("background:transparent;")
		self.setStyleSheet(open('temas/default.css', 'r', encoding = 'utf-8').read())

		self.fr_redes.close()
		self.hora_fecha()
		# self.mover_ventana()

		self.bt_cerrar.clicked.connect(self.close)
		self.bt_redes.clicked.connect(self.ventana_redes)

		self.bt_ajustes.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
		self.bt_ajustes.customContextMenuRequested.connect(self.menu_contextual)

		# Creamos el menú contextual
		self.menu = QMenu(self)
		self.menu.setObjectName(u"menu")
		self.menu.setCursor(QCursor(Qt.CursorShape.PointingHandCursor))

		self.inicio_sistema = QAction('&Iniciar con el sistema')

		self.derecha_abajo = QAction('&Mover abajo a la derecha')
		# self.derecha_abajo = QPushButton("hhhhhh")
		self.izquierda_abajo = QAction('&Mover abajo a la izquierda')

		self.derecha_arriba = QAction('&Mover arriba a la derecha')
		self.izquierda_arriba = QAction('&Mover arriba a la izquierda')

		self.arriba_centrar = QAction('&Mover arriba al centro')
		self.abajo_centrar = QAction('&Mover abajo al centro')
		self.centrar = QAction('&Mover al centro')

		self.tema = QAction('&Cambiar tema al Gadget')
		self.tema_default = QAction('&Tema predeterminado del Gadget')

		# self.inicio_sistema.triggered.connect(self.iniciar_sistema)
		self.inicio_sistema.triggered.connect(self.on_chekeable)

		self.tema.triggered.connect(self.cambiar_tema)
		self.tema_default.triggered.connect(self.tema_predeterminado)
		
		self.inicio_sistema.setCheckable(True)

		# self.derecha_abajo.setShortcut(QKeySequence("alt+a"))
		# self.derecha_abajo.setShortcutVisibleInContextMenu(True)
		# self.izquierda_abajo.setCheckable(True)

		# self.derecha_arriba.setCheckable(True)
		# self.izquierda_arriba.setCheckable(True)

		# self.arriba_centrar.setCheckable(True)
		# self.abajo_centrar.setCheckable(True)

		self.menu.addAction(self.inicio_sistema)
		self.menu.addSeparator()
		self.menu.addAction(self.derecha_abajo)
		self.menu.addSeparator()
		self.menu.addAction(self.izquierda_abajo)
		self.menu.addSeparator()
		self.menu.addAction(self.derecha_arriba)
		self.menu.addSeparator()
		self.menu.addAction(self.izquierda_arriba)
		self.menu.addSeparator()
		self.menu.addAction(self.arriba_centrar)
		self.menu.addSeparator()
		self.menu.addAction(self.abajo_centrar)
		self.menu.addSeparator()
		self.menu.addAction(self.centrar)
		self.menu.addSeparator()
		self.menu.addAction(self.tema)
		self.menu.addSeparator()
		self.menu.addAction(self.tema_default)

		self.gm = self.geometry()
		self.ancho, self.alto = self.gm.width(), self.gm.height()
		self.pv = PosicionVentana(self, w=self.ancho, h=self.alto)
		self.derecha_abajo.triggered.connect(lambda:self.pv.abajo.derecha())
		self.izquierda_abajo.triggered.connect(lambda:self.pv.abajo.izquierda())
		self.derecha_arriba.triggered.connect(lambda:self.pv.arriba.derecha())
		self.izquierda_arriba.triggered.connect(lambda:self.pv.arriba.izquierda())
		self.arriba_centrar.triggered.connect(lambda:self.pv.arriba.centrar())
		self.abajo_centrar.triggered.connect(lambda:self.pv.abajo.centrar())
		self.centrar.triggered.connect(lambda:self.pv.centrar())

		# Conectamos los botones con sus metodos
		self.bt_tg.clicked.connect(self.url_tele)
		self.bt_tk.clicked.connect(self.url_tick)
		self.bt_wh.clicked.connect(self.url_wasa)
		self.bt_fb.clicked.connect(self.url_face)
		self.bt_ig.clicked.connect(self.url_inst)
		self.bt_tw.clicked.connect(self.url_twit)

		# Nombre de la aplicación
		self.lb_titulo.setText(u"<h4>&nbsp; Reloj Sinergia </h4>")

		self.lb_titulo.setToolTip(u"<p style=\"color: #fad609;  font-size: 23px; \">[ Gadget 🖥 ], accesorio para simplificar nuestra vida desde la digitalización.</p>")

		self.bt_redes.setToolTip(u"<p style=\"color: #fad609;  font-size: 23px; \">Si quieres contactar al creador dale click para ver sus redes sociales.</p> ")

		self.bt_ajustes.setToolTip(u"<p style=\"color: #fad609;  font-size: 23px; \">Dale click derecho para ver el menú contextual y ajustar la posición del Gadget 🖥.</p>")

		self.bt_cerrar.setToolTip(u"<p style=\"color: #fad609;  font-size: 23px; \">Cerrar Gadget 🖥</p>")
		
	# Metodo para abrir y cerrar la barra de redes sociales
	def ventana_redes(self):
		if self.fr_redes.isVisible():
			self.fr_redes.close()
			icon1 = QIcon()
			icon1.addFile(u":/img/mas.svg", QSize(), QIcon.Normal, QIcon.Off)
			self.bt_redes.setIcon(icon1)
		else:
			self.fr_redes.show()
			icon1 = QIcon()
			icon1.addFile(u":/img/menos.svg", QSize(), QIcon.Normal, QIcon.Off)
			self.bt_redes.setIcon(icon1)

		# Inicializamos el Timer para ver la hora y fecha
		self.timer = QTimer()
		self.timer.timeout.connect(self.hora_fecha)
		self.timer.start(1000)

	# Metodo para mostrar la hora y la fecha
	def hora_fecha(self):
		setlocale(LC_ALL, 'es_MX.UTF-8')
		hora = strftime ("%I:%M  %p")
		self.lb_hora.setText(hora)
		fecha = strftime ("%A, %d de %B del %Y ")
		self.lb_fecha.setText(fecha)

	# Metodos para los botones de redes sociales
	def url_tele(self):
		webbrowser.open("https://t.me/selenofobico")

	def url_inst(self):
		webbrowser.open("https://www.instagram.com/duquits")

	def url_wasa(self):
		webbrowser.open("https://wa.me/525613175036")

	def url_tick(self):
		webbrowser.open("http://tiktok.com/@selenofobico")

	def url_face(self):
		webbrowser.open("http://m.me/selenofobico")

	def url_twit(self):
		webbrowser.open("https://twitter.com/selenofobico")

	# Hacemos que sea movible sobreescribiendo metodos nativos
	def mousePressEvent(self, event):
		self.dragPos = event.globalPosition().toPoint()

	def mouseMoveEvent(self, event):
		self.move(self.pos() + event.globalPosition().toPoint() - self.dragPos)
		self.dragPos = event.globalPosition().toPoint()
		event.accept()
    # ############################################################

	def menu_contextual(self, point):
		# show context menu
		self.menu.exec(self.bt_ajustes.mapToGlobal(point))

	def on_chekeable(self):
		if self.inicio_sistema.isChecked():
			self.inicio_sistema.setText(" &Quitar del inicio")
			print('se clikeo')
		else:
			self.inicio_sistema.setText(" &Iniciar con el sistema")
			
			print('se desclikeo')


    # Metodo para iniciar el reloj con el sistema
	def iniciar_sistema(self):
		if self.inicio_sistema.isChecked():
			acceso_directo = 'Udeler.lnk'
			usuario = getuser()
			escritorio = r"\Desktop"
			barra = "\\"
			perfil = r"C:\Users" + barra + usuario + escritorio
			appdata = getenv('APPDATA')
			ruta_acceso_directo = path.join(perfil, acceso_directo)
			ruta_startup = path.join(appdata,r'Microsoft\Windows\Start Menu\Programs\Startup',acceso_directo)
			if path.exists(ruta_acceso_directo):
				copy(ruta_acceso_directo, ruta_startup)
				self.inicio_sistema.setToolTip('Se ha copiado el acceso directo 😊')

			print('Se hizo click')
		else:
			acceso_directo = 'Udeler.lnk'
			appdata = getenv('APPDATA')
			ruta_startup = path.join(appdata,r'Microsoft\Windows\Start Menu\Programs\Startup',acceso_directo)
			if path.exists(ruta_startup):
				remove(ruta_startup)
				self.inicio_sistema.setToolTip('Se ha quitado el acceso directo 🤔')

			print('Se quito el click')
	# *****************************************
	def cambiar_tema(self):
		self.setStyleSheet(open('temas/morado.css', 'r', encoding = 'utf-8').read())

	def tema_predeterminado(self):
		self.setStyleSheet(open('temas/default.css', 'r', encoding = 'utf-8').read())

