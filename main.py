# File: main.py
import sys
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import QApplication
from PySide6.QtCore import QFile, QIODevice, Qt

from src.rj_metodos import RjMetodos



if __name__ == "__main__":
    ap = QApplication([])
    sg = RjMetodos()
    sg.show()
    ap.setStyle('Fusion')
    ap.exec()