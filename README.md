# Gadget Reloj

## Tecnologias usadas Python 3 🐍 y PySide 6

### Gracias a todos aquellos a quienes tuvieron  a bien ayudarme a terminarlo, Tomas, Jose, Uno maS, Razsen

### Si falta alguien, mil disculpas 🐍

------------------------------------------
![ Portada 😎](img/reloj.png)

<!-- ![ Video 😎](https://youtu.be/_PSo2f41JjM) -->


![ Video de Muestra 😎](img/reloj.mp4)

<!-- <video width="520" height="340" controls>
  <source src="img/reloj.mp4" type="video/mp4">
</video> -->


<!-- [![](img/reloj.png)](img/reloj.mp4 "Mostrando como se usa el Gadget 😎") -->

<!-- <iframe class="video_container" width="1583" height="592" src="https://www.youtube.com/embed/_PSo2f41JjM" title="Video de muestra Reloj Sinergia  😎" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> -->



